/*
#############################################################################################################

  Archivo      : MathLib.c
  Proyecto     : Modulo Interfaz K
  Fecha Inicio : 01/Diciembre/2012


=============================================================================================================

  (c) Copyright SICOM, Sistemas y Controles MINED S.A. de C.V.

  SICOM, Sistemas y Controles MINED S.A. de C.V.

  http    : www.sicom.mx
  mail    : info@sicom.com

#############################################################################################################

 MathLib

#############################################################################################################
*/

#include "MathLib.h"
#include "Types.h"
//#include "WatchDog.h"

#include "math.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
//
//
////===========================================================================================================
////   Funcion     : MathLib_ByteBcdToHex
////
////   Description : Convierte el BCD en Hexadecimal.
////                      Nota: El Maximo numero de B es 0x99 --> 0x63
////                            Ejemplo: 0x59 --> 0x3B
////
////   Parametros  : unsigned char B
////   Returns     : unsigned char HEX
////
////   Autor       : Ricardo Garcia
////   Fecha       : Dic/2012
////===========================================================================================================
//UINT8 MathLib_ByteBcdToHex(UINT8 B)
//{
//UINT8 Unidades;
//
//  Unidades = B & 0x0F;
//  return (((B >> 4) * 10) + Unidades);
//}
//
////===========================================================================================================
////   Funcion     : MathLib_ByteHexToBcd
////
////   Description : Convierte el BCD en Hexadecimal
////
////                  Nota: El Maximo numero de B es 0x63 --> 0x99
////
////                  Ejemplo: 0x3B --> 0x59
////
////   Parametros  : unsigned char B
////   Returns     : unsigned char BCD
////
////   Autor       : Ricardo Garcia
////   Fecha       : Dic/2012
////===========================================================================================================
//UINT8 MathLib_ByteHexToBcd(UINT8 B)
//{
//UINT8 Decenas;
//  if(B > 99) return(0x99);
//  Decenas = B / 10;
//  Decenas = Decenas << 4;
//  Decenas += B %10;
//  return (Decenas);
//}
//
//
////===========================================================================================================
////   Funcion     : MathLib_CharBcdToHex
////
////   Description : Convierte el BCD en Hexadecimal
////            Nota: El Maximo numero de B es 0x99 --> 0x63
////
////                  Ejemplo: 0x3B --> 0x59
////
////   Parametros  : unsigned char B
////   Returns     : unsigned char HEX
////
////   Autor       : VHAG y RGG
////   Fecha       : Dic/2012
////===========================================================================================================
//UINT8 MathLib_CharBcdToHex(UINT8 B)
//{
//  UINT8 Unidades;
//  UINT8 Decenas;
//
//  Unidades = B & 0x0F;
//  if (Unidades > 9) return(0xFF);
//  Decenas = B >> 4;
//  if (Decenas > 9) return(0xFF);
//  return ((Decenas * 10) + Unidades);
//}
//
//
//
//
////===========================================================================================================
////   Funcion     : MathLib_BcdToHex
////
////   Description : Convierte 4 Char de BCD a HEX de manera independiente
////        :modificada para Endianness little
////   Parametros  : unsigned char *Ptr
////   Returns     : Nothing
////
////   Autor       : Ricardo Garcia,Illan
////   Fecha       : Dic/2012
////===========================================================================================================
//void MathLib_BcdToHex(UINT8 *Ptr, UINT8 Size)
//{
//  Ptr += Size-1;
//  while(Size--)
//  {
//    RESET_WDOG;
//    *Ptr = MathLib_ByteBcdToHex(*Ptr);
//    *Ptr --;
//  }
//}
//
////===========================================================================================================
////   Funcion     : MathLib_HexToBcd
////
////   Description : Convierte 3 Char de BCD a HEX de manera independiente
////            modificada para Endianness little
////   Parametros  : unsigned char *Ptr
////   Returns     : Nothing
////
////   Autor       : Ricardo Garcia,Illan
////   Fecha       : Dic/2012
////===========================================================================================================
//void MathLib_HexToBcd(UINT8 *Ptr, UINT8 Size)
//{
//  Ptr += Size-1;
//
//  while(Size--)
//  {
//    RESET_WDOG;
//    *Ptr = MathLib_ByteHexToBcd(*Ptr);
//    Ptr--;
//  }
//}
//
//
////===========================================================================================================
////   Funcion     : MathLib_HexAsc1
////
////   Description : Convierte el nible menos significativo de B a Ascii
////
////   Parametros  : unsigned char B
////   Returns     : unsigned char ASCII
////
////   Autor       : Ricardo Garcia
////   Fecha       : Dic/2012
////===========================================================================================================
//UINT8 MathLib_HexAsc1 (UINT8  B)
//{
//  B &= 0x0F;
//  if(B < 10) return(B + '0');
//  else return(B + 'A' -10);
//}
//
////===========================================================================================================
////   Funcion     : MathLib_HexAsc2
////
////   Description : Convierte los dos nibles de B a Ascii y los regresa como un integer
////
////   Parametros  : unsigned char B
////   Returns     : unsigned int
////
////   Autor       : Ricardo Garcia
////   Fecha       : Dic/2012
////===========================================================================================================
//UINT16 MathLib_HexAsc2(UINT8 B)
//{
//UINT16 DatoAscii = 0;
//UINT8 *Ptr;
//  Ptr = (UINT8*)&DatoAscii;
//  *Ptr = MathLib_HexAsc1(B );
//  Ptr++;
//  *Ptr = MathLib_HexAsc1(B>>4);
//  return(DatoAscii);
//}
//
//
//
////===========================================================================================================
////   Funcion     : MathLib_HexAsc
////
////   Description : Convierte "Size" bytes apuntados por "Dato" en ASCII y lo concatena a partir del apuntador "String"
////
////   Parametros  :  unsigned char *String
////                  unsigned char *Dato
////   Returns     :  unsigned char Size
////
////   Autor       : Ricardo Garcia
////   Fecha       : Dic/2012
////===========================================================================================================
//void MathLib_HexToAsc (UINT8 *String,UINT8 *Dato, UINT8 Size)
//{
//  UINT8 i = (Size*2)-1;
//    while(Size--)
//    {
//      RESET_WDOG;
//      String[i--] = MathLib_HexAsc1(*Dato);
//      String[i--] = MathLib_HexAsc1(*Dato>>4);
//      Dato++;
//    }
//}
///*
//void MathLib_HexToAsc (UINT8 *String,UINT8 *Dato, UINT8 Size)
//{
//UINT8 i = 0;
//  while(Size--)
//  {
//    String[i++] = MathLib_HexAsc1(*Dato >> 4);
//    String[i++] = MathLib_HexAsc1(*Dato++);
//  }
//}
//*/
////===========================================================================================================
////   Funcion     : MathLib_CharToAsc
////
////   Description : Convierte "Size" bytes apuntados por "Dato" en ASCII y lo concatena a partir del apuntador "String"
////
////   Parametros  :  unsigned char *String
////                  unsigned char *Dato
////   Returns     :  unsigned char Size
////
////   Autor       : Ricardo Garcia
////   Fecha       : Dic/2012
////===========================================================================================================
//void MathLib_CharToAsc(UINT8 *String, UINT8 cDato)
//{
//  MathLib_HexToAsc(String, (UINT8*)&cDato, sizeof(char));
//}
//
////===========================================================================================================
////   Funcion     : MathLib_IntToAsc
////
////   Description : Convierte "Size" bytes apuntados por "Dato" en ASCII y lo concatena a partir del apuntador "String"
////
////   Parametros  :  unsigned char *String
////                  unsigned char *Dato
////   Returns     :  unsigned char Size
////
////   Autor       : Ricardo Garcia
////   Fecha       : Dic/2012
////===========================================================================================================
//void MathLib_IntToAsc(UINT8 *String, UINT16  iDato)
//{
//  MathLib_HexToAsc(String, (UINT8*)&iDato, sizeof(short));
//}
//
////===========================================================================================================
////   Funcion     : MathLib_LongToAsc
////
////   Description :
////
////   Parametros  :  unsigned char *String
////                  unsigned char *Dato
////   Returns     :  unsigned char Size
////
////   Autor       : Ricardo Garcia
////   Fecha       : Dic/2012
////===========================================================================================================
//void MathLib_LongToAsc(UINT8 *String, UINT32 lDato)
//{
//  MathLib_HexToAsc(String, (UINT8*)&lDato, sizeof(long));
//}
//
////===========================================================================================================
////   Funcion     : MathLib_FloatToAsc
////
////   Description :
////
////   Parametros  :  unsigned char *String
////                  unsigned char *Dato
////   Returns     :  unsigned char Size
////
////   Autor       : Ricardo Garcia
////   Fecha       : Dic/2012
////===========================================================================================================
//void MathLib_FloatToAsc(UINT8 *String, float fDato)
//{
//  MathLib_HexToAsc(String, (UINT8*)&fDato, sizeof(float));
//}
//
//
////===========================================================================================================
////   Funcion     : MathLib_AscToByte
////
////   Description : Convierte el primer byte de la cadena en nible HEX Si los caracteres no son validos el Byte = FF
////
////   Parametros  :  unsigned char Dato
////   Returns     :  unsigned char Hex
////
////   Autor       : Ricardo Garcia
////   Fecha       : Dic/2012
////===========================================================================================================
//unsigned char MathLib_AscToByte(UINT8 Dato)
//{
//  if (Dato > 'F') {return(0xFF);}
//  else
//  {  if ((Dato >= 'A') && (Dato <= 'F')) {return(Dato - 0x37);}
//    else
//    {
//      if((Dato >= '0') && (Dato <= '9')) {return(Dato - 0x30);}
//      else {return(0xFF);}
//    }
//  }
//}
//
////===========================================================================================================
////   Funcion     : MathLib_AscToChar
////
////   Description : Convierte un texto numerico de 2 ASCII a Hexadecimal
////
////   Parametros  :  unsigned char *String
////   Returns     :  unsigned char B
////
////   Autor       : Ricardo Garcia
////   Fecha       : Dic/2012
////===========================================================================================================
//UINT8 MathLib_AscToChar(UINT8 *String)
//{
//  UINT8 B;
//
//  B = MathLib_AscToByte(String[0]);
//  B = B << 4;
//  B += MathLib_AscToByte(String[1]);
//  return(B);
//}
//
////===========================================================================================================
////   Funcion     : MathLib_AscToHex
////
////   Description : Convierte un texto numerico de tama�o Size a Hexadecimal
////
////   Parametros  :  unsigned char *PtrHex
////                  unsigned char *PtrAsc
////                  unsigned char Size
////   Returns     :  Nothing
////
////   Autor       : Ricardo Garcia
////   Fecha       : Dic/2012
////===========================================================================================================
//void MathLib_AscToHex(UINT8 *PtrHex,UINT8 *PtrAsc, UINT8 Size)
//{
//UINT8 i = 0;
//UINT8 C = Size-1;
//  while(i < Size)
//  {
//    RESET_WDOG;
//    PtrHex[C] = MathLib_AscToChar(&PtrAsc[i * 2]);
//    i++;
//    C--;
//  }
//
//}
//
////===========================================================================================================
////   Funcion     : MathLib_AscToInt
////
////   Description : Convierte un Texto numerico de 4 ASCII a Hexadecimal y lo regresa en una varible de tipo unsigned int
////
////   Parametros  :  unsigned char *String
////   Returns     :  unsigned int
////
////   Autor       : Ricardo Garcia
////   Fecha       : Dic/2012
////===========================================================================================================
//UINT16 MathLib_AscToInt(UINT8 *String)
//{
//UINT16 IData;
//
//  MathLib_AscToHex((UINT8*)&IData, String, sizeof(IData));
//  return(IData);
//}
//
////===========================================================================================================
////   Funcion     : MathLib_AscToLong
////
////   Description : Convierte un Texto numerico de 8 ASCII a Hexadecimal y lo regresa en una varible de tipo unsigned long
////
////   Parametros  :  unsigned char *String
////   Returns     :  unsigned long
////
////   Autor       : Ricardo Garcia
////   Fecha       : Dic/2012
////===========================================================================================================
//UINT32 MathLib_AscToLong(UINT8 *String)
//{
//  UINT32 LData;
//
//  MathLib_AscToHex((UINT8*)&LData, String, sizeof(LData));
//  return(LData);
//}
//
////===========================================================================================================
////   Funcion     : MathLib_AscToFloat
////
////   Description : Convierte un Texto numerico de 8 ASCII a Hexadecimal y lo regresa en una varible de tipo float
////
////   Parametros  :  unsigned char *String
////   Returns     :  float FData
////
////   Autor       : Ricardo Garcia
////   Fecha       : Dic/2012
////===========================================================================================================
//float MathLib_AscToFloat(UINT8 *String)
//{
//float FData;
//
//  MathLib_AscToHex((UINT8*)&FData, String, sizeof(FData));
//  return(FData);
//}
//
////===========================================================================================================
////   Funcion     : MathLib_AscToFloat
////
////   Description : Convierte un Texto numerico de 16 ASCII a Hexadecimal y lo regresa en una varible de tipo double
////
////   Parametros  :  unsigned char *String
////   Returns     :  dlouble DData
////
////   Autor       : Ricardo Garcia
////   Fecha       : Dic/2012
////===========================================================================================================
//double MathLib_AscToDouble(UINT8 *String)
//{
//double DData;
//
//  MathLib_AscToHex((UINT8*)&DData, String, sizeof(DData));
//  return(DData);
//}
//
////===========================================================================================================
////   Funcion     : MathLib_AscToBcd
////
////   Description : Regresa el Char en BCD
////
////   Parametros  :  unsigned char *String
////   Returns     :  unsigned char BCD
////
////   Autor       : Ricardo Garcia
////   Fecha       : Dic/2012
////===========================================================================================================
//UINT8 MathLib_AscToBcd (UINT8 *String)
//{
//  UINT8 CNum = 0;
//  CNum = String[strlen(String) - 1] - 0x30;
//  CNum += ((String[strlen(String) - 2] - 0x30) << 4);
//  return(CNum);
//}
//
//
////===========================================================================================================
////   Funcion     : MathLib_RoundFloat
////
////   Description : Redondea un valor tipo float a n Decimales y lo regresa
////
////   Parametros  :  float FNum
////                  char Decimales
////   Returns     :  float FNum
////
////   Autor       : VHAG y RGG
////   Fecha       : Dic/2012
////===========================================================================================================
//float MathLib_RoundFloat(float fNum, unsigned char u8Decimales)
//{
//float fMul;
//float fEnt ;
//float fDec ;
//float fAux ;
//float fSum ;
//
//  fMul = 1 ;
//  if(u8Decimales >5) u8Decimales = 5 ;
//  while(u8Decimales-- != 0)
//  {
//    RESET_WDOG;
//    fMul *= 10 ;
//
//  }
//  fSum = 1 / fMul ;
//  fAux = fNum * fMul;
//  fEnt = floorf(fAux) ;
//    RESET_WDOG;
//  fDec = fAux - fEnt ;
//  if(fDec >= 0.5)
//  {
//    fNum += fSum ;
//  }
//  fAux = fDec * fSum ;
//  fNum -= fAux ;
//  return(fNum);
//}
//
//
////===========================================================================================================
////   Funcion     : MathLib_RoundDoubleAux
////
////   Description : Regresa el Char en BCD
////
////   Parametros  :  unsigned char *String
////   Returns     :  unsigned char BCD
////
////   Autor       : VHAG y RGG
////   Fecha       : Dic/2012
////===========================================================================================================
//double MathLib_RoundDouble(double dNum, unsigned char u8Decimales)
//{
//double dMul ;
//double dEnt ;
//double dDec ;
//double dAux ;
//double dSum ;
//
//  dMul = 1 ;
//  if(u8Decimales >5) u8Decimales = 5 ;
//  while(u8Decimales-- != 0)
//  {
//    dMul *= 10 ;
//    RESET_WDOG;
//  }
//  dSum = 1 / dMul ;
//  dAux = dNum * dMul;
//  dEnt = floor(dAux) ;
//  RESET_WDOG;
//  dDec = dAux - dEnt ;
//  if(dDec >= 0.5)
//  {
//    dNum += dSum ;
//  }
//  dAux = dDec * dSum ;
//  dNum -= dAux ;
//  return(dNum);
//}
//
////===========================================================================================================
////   Funcion     : MathLib_Residuo
////
////   Description : Regresa el residuo de la division de 2 int
////
////   Parametros  :  unsigned int IDato
////                  unsigned int IDiv
////   Returns     :  unsigned int MathLib_Residuo
////
////   Autor       : Ricardo Garcia
////   Fecha       : Dic/2012
////===========================================================================================================
//UINT32 MathLib_Residuo(UINT32 LDato, UINT32 LDiv)
//{
//  UINT32 Cociente = 0;
//
//  Cociente = LDato / LDiv;
//  Cociente *= LDiv;
//  return(LDato - Cociente);
//}
//
////===========================================================================================================
////   Funcion     : MathLib_SumaBcdChar
////
////   Description : Regresa el residuo de la division de 2 int
////
////   Parametros  :  unsigned char Dato1
////                  unsigned char Dato2
////   Returns     :  unsigned char SUMA
////
////   Autor       : Ricardo Garcia
////   Fecha       : Dic/2012
////===========================================================================================================
//UINT8 MathLib_SumaBcdChar(UINT8 Dato1, UINT8 Dato2)
//{
//  UINT8 Suma;
//  UINT8 DIAL;
//  UINT8 DIAH;
//  UINT8 DIA_CREDITOL;
//  UINT8 DIA_CREDITOH;
//
//  DIA_CREDITOL = Dato1 & 0x0F;
//  DIA_CREDITOH = Dato1 >> 4;
//  DIAL = Dato2 & 0x0F;
//  DIAH = Dato2 >> 4;
//
//  DIAL += DIA_CREDITOL;
//  if (DIAL > 9){ DIAL += 6; DIAL&= 0x0F; DIA_CREDITOH++;}
//
//  DIAH += DIA_CREDITOH;
//  if (DIAH > 9){ DIAH += 6; DIAH&= 0x0F;}
//  Suma = DIAL + (DIAH << 4);
//  return(Suma);
//
//}
//
////===========================================================================================================
////   Funcion     : MathLib_RestaBcdChar
////
////   Description :
////
////   Parametros  :  unsigned char Dato1
////                  unsigned char Dato2
////   Returns     :  unsigned char RESTA
////
////   Autor       : Ricardo Garcia
////   Fecha       : Dic/2012
////===========================================================================================================
//UINT8 MathLib_RestaBcdChar(UINT8 Dato1, UINT8 Dato2)
//{
//  UINT8 Resta;
//  INT8 DIAL;
//  INT8 DIAH;
//    INT8 DIA_CREDITOL;
//    INT8 DIA_CREDITOH;
//
//  DIAL = Dato1 & 0x0F;
//  DIAH = Dato1 >> 4;
//  DIA_CREDITOL = Dato2 & 0x0F;
//  DIA_CREDITOH = Dato2 >> 4;
//
//  DIAL -= DIA_CREDITOL;
//  if (DIAL < 0){ DIAL -= 6; DIAL&= 0x0F; DIA_CREDITOH++;}
//
//  DIAH -= DIA_CREDITOH;
//  if (DIAH < 0){ DIAH -= 6; DIAH&= 0x0F;}
//  Resta = DIAL + (DIAH << 4);
//  return(Resta);
//
//}
//
//
////===========================================================================================================
////   Funcion     : MathLib_StringClear
////
////   Description :
////
////   Parametros  :  unsigned char *Ptr_Long
////   Returns     :  Nothing
////
////   Autor       : Ricardo Garcia
////   Fecha       : Dic/2012
////===========================================================================================================
//void MathLib_StringClear(UINT8*str)
//{
//  UINT8 *Ptr;
//  Ptr = strlen(str) + str;
//  while(Ptr-- != str+1)
//  {
//    RESET_WDOG;
//    if(Ptr[0] == ' ') Ptr[0] = 0;
//    else break;
//  }
//
//}
////===========================================================================================================
////   Funcion     : MathLib_StringClear
////
////   Description :
////
////   Parametros  :  unsigned char *Ptr_Long
////   Returns     :  Nothing
////
////   Autor       : Ricardo Garcia
////   Fecha       : Dic/2012
////===========================================================================================================
//UINT16       MathLib_StringCopy                   (UINT8 *str_destino, UINT8 *str_fuente)
//{
//UINT16 u16Count = 0;
//	  while(*str_fuente)
//	  {
//		RESET_WDOG;
//	    *str_destino++ = *str_fuente;
//	    str_fuente++;
//	    u16Count++;
//	  }
//	  *str_destino = 0;
//	  return u16Count;
//}
//
////===========================================================================================================
////   Funcion     : MathLib_StringCopyMax
////
////   Description : Copia caracteres de un arreglo de bytes a otro hasta aun maximo de caracteres
////                 indicados en <Count> o hasta encontrar un caracter null.
////
////   Parametros  : str_d : Apuntador a cadena destino
////                 str_f : Apuntador a cadena fuente
////                 Count : Maximo numero de bytes a copiar
////   Returns     :
////
////   Autor       : Ricardo Garcia
////   Fecha       : Abril/2013
////===========================================================================================================
//void MathLib_StringCopyMax(UINT8 *str_destino, UINT8 *str_fuente, UINT32 Count)
//{
//  while(Count--)
//  {
//  RESET_WDOG;
//    *str_destino++ = *str_fuente;
//    if(*str_fuente++ == 0) return;
//  }
//  *str_destino = 0;
//}
////===========================================================================================================
////   Funcion     : MathLib_StringNCopy
////
////   Description : Copia caracteres de un arreglo de bytes a otro hasta aun maximo de caracteres
////                 indicados en <Count> o hasta encontrar un caracter null.
////
////   Parametros  : str_d : Apuntador a cadena destino
////                 str_f : Apuntador a cadena fuente
////                 Count : Maximo numero de bytes a copiar
////   Returns     :
////
////   Autor       : Ricardo Garcia
////   Fecha       : Abril/2013
////===========================================================================================================
//void MathLib_StringNCopy(UINT8 *str_destino, UINT8 *str_fuente, UINT32 Count)
//{
//  while(Count--)
//  {
//    RESET_WDOG;
//    *str_destino++ = *str_fuente++;
//  }
//}
////===========================================================================================================
////   Funcion     : MathLib_StringCopyMaxEndNULL
////
////   Description : Copia caracteres de un arreglo de bytes a otro hasta aun maximo de caracteres
////                 indicados en <Count> o hasta encontrar un caracter null.
////                 Pone caracter NULL al final del string
////
////   Parametros  : str_d : Apuntador a cadena destino
////                 str_f : Apuntador a cadena fuente
////                 Count : Maximo numero de bytes a copiar
////   Returns     :
////
////   Autor       : Arturo Garcia
////   Fecha       : Abril/2015
////===========================================================================================================
//void MathLib_StringCopyMaxEndNULL(UINT8 *str_destino, UINT8 *str_fuente, UINT32 Count)
//{
//  MathLib_StringCopyMax(str_destino, str_fuente, Count) ;
//  str_destino[Count-1] = 0 ;
//}
//
//
////===========================================================================================================
////   Funcion     : MathLib_ConcatenarStrings
////
////   Description : concatena 2 string
////          *el strig destino debe de ser lo suficientemente grande para poder soportar el
////            tamano de los 2 string juntos
////   Parametros  : str_d : Apuntador a cadena destino
////                 str_f : Apuntador a cadena fuente
////
////   Returns     :
////
////   Autor       : PAIG
////   Fecha       : julio/2016
////===========================================================================================================
//void        MathLib_ConcatenarStrings          (UINT8* str_destino ,UINT8 *str_fuente)
//{
//  UINT16 i,j;
//    for(i=0; str_destino[i] != '\0'; ++i){RESET_WDOG;}  /* i contains length of string s1. */
//    for(j=0; str_fuente [j] != '\0'; ++j, ++i)
//    {
//      RESET_WDOG;
//      str_destino[i] = str_fuente[j];
//    }
//      str_destino[i]='\0';
//}
//
//
////===========================================================================================================
////   Funcion     : MathLib_ConcatenarStrings
////
////   Description : concatena 2 string
////          *el strig destino debe de ser lo suficientemente grande para poder soportar el
////            tamano de los 2 string juntos
////   Parametros  : str_d : Apuntador a cadena destino
////                 str_f : Apuntador a cadena fuente
////
////   Returns     :
////
////   Autor       : PAIG
////   Fecha       : julio/2016
////===========================================================================================================
//void   MathLib_SustituirNCaracterEnStrings    (UINT8* str ,UINT8 u8CaracterBuscado,UINT8 u8NuevoCaracter,UINT32 u32NumeroDeCaracteres)
//{
//UINT16 u16Iterador = 0;
//  while(str[u16Iterador] != 0)
//  {
//     RESET_WDOG;
//     if(str[u16Iterador] == u8CaracterBuscado)
//     {
//       str[u16Iterador] = u8NuevoCaracter;
//       u32NumeroDeCaracteres --;
//     }
//     u16Iterador ++;
//     if(u32NumeroDeCaracteres == 0) break;
//  }
//}
//
//===========================================================================================================
//   Funcion     : MathLib_BitMask
//
//   Description : Regresa la mascar del Bit
//
//   Parametros  :  unsigned char Bit
//   Returns     :  unsigned char Mascara
//
//   Autor       : Ricardo Garcia
//   Fecha       : Dic/2012
//===========================================================================================================
UINT32 MathLib_BitMask (UINT32 Bit)
{
  switch(Bit)
  {
    case 0:
      Bit = BIT0;
      break;
    case 1:
      Bit = BIT1;
      break;
    case 2:
      Bit = BIT2;
      break;
    case 3:
      Bit = BIT3;
      break;
    case 4:
      Bit = BIT4;
      break;
    case 5:
      Bit = BIT5;
      break;
    case 6:
      Bit = BIT6;
      break;
    case 7:
      Bit = BIT7;
      break;
    case 8:
      Bit = BIT8;
      break;
    case 9:
      Bit = BIT9;
      break;
    case 10:
      Bit = BIT10;
      break;
    case 11:
      Bit = BIT11;
      break;
    case 12:
      Bit = BIT12;
      break;
    case 13:
      Bit = BIT13;
      break;
    case 14:
      Bit = BIT14;
      break;
    case 15:
      Bit = BIT15;
      break;
    case 16:
      Bit = BIT16;
      break;
    case 17:
      Bit = BIT17;
      break;
    case 18:
      Bit = BIT18;
      break;
    case 19:
      Bit = BIT19;
      break;
    case 20:
      Bit = BIT20;
      break;
    case 21:
      Bit = BIT21;
      break;
    case 22:
      Bit = BIT22;
      break;
    case 23:
      Bit = BIT23;
      break;
    case 24:
      Bit = BIT24;
      break;
    case 25:
      Bit = BIT25;
      break;
    case 26:
      Bit = BIT26;
      break;
    case 27:
      Bit = BIT27;
      break;
    case 28:
      Bit = BIT28;
      break;
    case 29:
      Bit = BIT29;
      break;
    case 30:
      Bit = BIT30;
      break;
    case 31:
      Bit = BIT31;
      break;
    default:
      break;
  }
  return(Bit) ;
}

//===========================================================================================================
//   Funcion     : MathLib_BitMaskNot
//
//   Description : Regresa la mascar del Bit
//
//   Parametros  :  unsigned char Bit
//   Returns     :  unsigned char Mascara
//
//   Autor       : Ricardo Garcia
//   Fecha       : Dic/2012
//===========================================================================================================

UINT32 MathLib_BitMaskNot (UINT32 Bit)
{

  switch(Bit)
  {
    case 0:
      Bit = ~BIT0;
      break;
    case 1:
      Bit = ~BIT1;
      break;
    case 2:
      Bit = ~BIT2;
      break;
    case 3:
      Bit = ~BIT3;
      break;
    case 4:
      Bit = ~BIT4;
      break;
    case 5:
      Bit = ~BIT5;
      break;
    case 6:
      Bit = ~BIT6;
      break;
    case 7:
      Bit = ~BIT7;
      break;
    case 8:
      Bit = ~BIT8;
      break;
    case 9:
      Bit = ~BIT9;
      break;
    case 10:
      Bit = ~BIT10;
      break;
    case 11:
      Bit = ~BIT11;
      break;
    case 12:
      Bit = ~BIT12;
      break;
    case 13:
      Bit = ~BIT13;
      break;
    case 14:
      Bit = ~BIT14;
      break;
    case 15:
      Bit = ~BIT15;
      break;
    case 16:
      Bit = ~BIT16;
      break;
    case 17:
      Bit = ~BIT17;
      break;
    case 18:
      Bit = ~BIT18;
      break;
    case 19:
      Bit = ~BIT19;
      break;
    case 20:
      Bit = ~BIT20;
      break;
    case 21:
      Bit = ~BIT21;
      break;
    case 22:
      Bit = ~BIT22;
      break;
    case 23:
      Bit = ~BIT23;
      break;
    case 24:
      Bit = ~BIT24;
      break;
    case 25:
      Bit = ~BIT25;
      break;

    case 26:
      Bit = ~BIT26;
      break;
    case 27:
      Bit = ~BIT27;
      break;
    case 28:
      Bit = ~BIT28;
      break;
    case 29:
      Bit = ~BIT29;
      break;
    case 30:
      Bit = ~BIT30;
      break;
    case 31:
      Bit = ~BIT31;
      break;
    default:
      break;

  }
  return(Bit);
}
//
////===========================================================================================================
////   Funcion     : MathLib_BitMaskOff
////
////   Description : Regresa la mascar de los Bits
////
////   Parametros  :  unsigned char Bit
////   Returns     :  unsigned char Mascara
////
////   Autor       : Ricardo Garcia
////   Fecha       : Dic/2012
////===========================================================================================================
//UINT8 MathLib_BitMaskOff (UINT8 Bit)
//{
//  switch(Bit)
//  {
//    case 0:
//      Bit = 0xFF;
//      break;
//    case 1:
//      Bit = 0x01;
//      break;
//    case 2:
//      Bit = 0x03;
//      break;
//    case 3:
//      Bit = 0x07;
//      break;
//    case 4:
//      Bit = 0x0F;
//      break;
//    case 5:
//      Bit = 0x1F;
//      break;
//    case 6:
//      Bit = 0x3F;
//      break;
//    case 7:
//      Bit = 0x7F;
//      break;
//    case 8:
//      Bit = 0xFF;
//      break;
//    default:
//      break;
//  }
//  return(Bit);
//}
//
////===========================================================================================================
////  Funcion     : Mathlib_InicializarAleatorios
////
////  Description :
////
////  Parametros  :
////  Returns     :
////
////  Autor       : AGS
////  Fecha       : Nov/2015
////===========================================================================================================
//void Mathlib_InicializarAleatorios(unsigned int u16Semilla)
//{
//  srand(u16Semilla) ;
//}
//
//
////===========================================================================================================
////   Funcion     : MathLib_DivisionArray
////
////   Description : Divide un numero representado en un array en formato ASCII y coloca el cociente en otro
////                 array en formato ASCII
////
////   Parametros  :
////   Returns     :
////
////   Autor       : AGS
////   Fecha       : Nov/2015
////===========================================================================================================
//UINT8 MathLib_DivisionArray(UINT8 *pu8Dividendo, UINT8 u8Divisor, UINT8 *pu8Cociente, UINT8 *pu8Residuo)
//{
//  UINT8 u8Temp = 0;
//  UINT16 i = 0 ;
//  UINT16 j = 0 ;
//  UINT8 u8Aux = FALSE ;
//
//  while(pu8Dividendo[i++])
//  {
//    RESET_WDOG;
//     u8Temp = u8Temp * 10 + MathLib_AscToByte(pu8Dividendo[i]) ;
//     if(u8Temp < u8Divisor)
//     {
//         pu8Cociente[j++] = MathLib_HexAsc1(0) ;
//     }
//     else
//     {
//         u8Aux = TRUE ;
//         pu8Cociente[j++] = MathLib_HexAsc1(u8Temp / u8Divisor) ;
//         u8Temp = u8Temp % u8Divisor;
//     }
//  }
//  pu8Cociente[j] = 0x00 ;
//  *pu8Residuo = MathLib_HexAsc1(u8Temp) ;
//  return(u8Aux) ;
//}
//
////===========================================================================================================
////   Funcion     : MathLib_ConvierteABaseSuperior
////
////   Description : Convierte la representacion en string ASCII de un numero en un base a su representacion
////                 en string ASCII en otra base mediante el algoritmo de divisiones sucesivas de los cocientes
////                 entre la base y tomando como datos los residuos
////
////   Parametros  :
////   Returns     :
////
////   Autor       : AGS
////   Fecha       : Nov/2015
////===========================================================================================================
//void MathLib_ConvierteABaseSuperior(UINT8 *pu8NumeroOriginal, UINT8 u8Base, UINT8 *pu8NumeroConvertido, UINT8 *pu8Cociente, UINT8 *pu8Dividendo, UINT8 *pu8Aux)
//{
//  UINT8 i = 0 ;
//  UINT8 j = 0 ;
//  UINT8 u8Residuo ;
//  UINT8 u8Aux = TRUE ;
//
//  strcpy(pu8Cociente, pu8NumeroOriginal) ;
//
//  while(u8Aux == TRUE)
//  {
//    RESET_WDOG;
//    strcpy(pu8Dividendo, pu8Cociente) ;
//    u8Aux = MathLib_DivisionArray(pu8Dividendo, u8Base, pu8Cociente, &u8Residuo) ;
//    pu8Aux[i] = u8Residuo ;
//    i ++ ;
//  }
//
//  pu8Aux[i] = 0x00 ;
//  i -- ;
//
//  while(i >= 0)
//  {
//    RESET_WDOG;
//    pu8NumeroConvertido[j] = pu8Aux[i] ;
//    j ++ ;
//    i -- ;
//  }
//
//  pu8NumeroConvertido[j] = 0x00 ;
//}
//
//
////===========================================================================================================
////   Funcion     : MathLib_StringConcatenaNibbles
////
////   Description : Concatena en pares los valores representados en ASCII de un string
////
////   Parametros  :
////   Returns     :
////
////   Autor       : AGS
////   Fecha       : Nov/2015
////===========================================================================================================
//void MathLib_StringConcatenaNibbles(UINT8 *pu8String)
//{
//  UINT8 i = 0;
//  UINT8 u8Aux1 ;
//  UINT8 u8Aux2 ;
//
//  while(pu8String[i*2])
//  {
//    /*
//    u8Aux1 = MathLib_AscToByte(pu8String[i*2]) << 4 ;
//    u8Aux2 = MathLib_AscToByte(pu8String[(i*2)+1]) ;
//    pu8String[i] = u8Aux1 + u8Aux2 ;
//    i++ ;
//    */
//    RESET_WDOG;
//    pu8String[i] = (MathLib_AscToByte(pu8String[i*2]) << 4) + MathLib_AscToByte(pu8String[(i*2)+1]) ;
//    i++;
//  }
//}
//
////===========================================================================================================
////   Funcion     : MathLib_LittleEndianToBigEndian
////
////   Description :
////
////
////   Parametros  :
////
////
////   Returns     :
////
////   Autor       : Arturo Garcia
////   Fecha       : Julio/2015
////===========================================================================================================
//unsigned char MathLib_LittleEndianToBigEndian(unsigned char *Ptr, unsigned char Size, unsigned char *Result)
//{
//  Ptr += Size ;
//  Ptr -- ;
//  while(Size--)
//  {
//    RESET_WDOG;
//    *Result = *Ptr ;
//    Result ++ ;
//    Ptr -- ;
//  }
//}
//
////===========================================================================================================
////   Funcion     : MathLib_LittleEndianToBigEndian2
////
////   Description : al pasar cualquier tipo de dato (int long char short)  se invierte el dato y al salir de la funcion se retorna con un endian diferente
////                 ( la funcion puede se usada cuando un dato tiene un enddian diferente al deciado)
////                  (No se puede usar con float o double)
////                 ejemplo:
////                 Antes  UINT32 Dato = 0x11223344;
////                           MathLib_LittleEndianToBigEndian2(&u32Dato ,sizeof(UINT32 ));
////                 Despues  Dato = 0x44332211;
////
////   Parametros  :UINT8 *pDato Direccion del Dato al que se decia realizar el cambio de endianees
////                Size Tamano del  Dato al que se decia realizar el cambio de endianees
////
////   Returns     :void
////
////   Autor       : PedroAntonio Illan Gonzalez
////   Fecha       : Abril/2015
////===========================================================================================================
//void MathLib_LittleEndianToBigEndianData(UINT8 *pDato, UINT8 Size)
//{
//UINT32 u32Dato=0;
//UINT8  *pDato2 ;
//UINT8 Contador = Size;
//pDato2 = (UINT8*)&u32Dato;
//
//  while (Contador--)
//  {
//    RESET_WDOG;
//    *pDato2++ = *pDato++;
//  }
//  pDato2 = (UINT8*)&u32Dato ;
//  pDato--;
//
//  while(Size--)
//  {
//    RESET_WDOG;
//    *pDato = *pDato2 ;
//    pDato -- ;
//    pDato2 ++ ;
//  }
//}
//
////===========================================================================================================
////   Funcion     : MathLib_CastData
////
////   Description : Recupera el dato de la dirreccion proporcionada , y lo retorna el dato en el puntero
////
////   Parametros  : Address: direccion de donde se obtendra el dato,
////             pu8Data: puntero donde se almacenara el dato extraido
////   Returns     : Nothing
////
////   Autor       : PAIG
////   Fecha       : Febrero/2015
////===========================================================================================================
//void  MathLib_CastData(UINT32 Address, UINT8 u8Size, UINT8 *pu8Data)
//{
//  UINT8  *pu8;
//  UINT8 Contador = u8Size;
//  pu8 = (UINT8*)Address;
//  while(Contador--  )
//  {
//    RESET_WDOG;
//    *pu8Data = *pu8;
//    pu8Data++;
//    pu8++;
//  }
//
//}
//
//
////===========================================================================================================
////   Funcion     : MathLib_QuitarCaracteresNoASCII
////
////   Description :
////
////   Parametros  :
////   Returns     :
////
////   Autor       : AGS
////   Fecha       : Mayo/2016
////===========================================================================================================
//void MathLib_QuitarCaracteresDeControl(UINT8 *Str)
//{
//  while(*Str)
//  {
//    RESET_WDOG;
//    if(*Str == 0x08)
//    {
//      *Str = ' ' ;
//      Str ++ ; *Str = ' ' ;
//      Str ++ ; *Str = ' ' ;
//    }
//    else Str ++ ;
//
//  }
//}
//
////===========================================================================================================
////   Funcion     : MathLib_QuitarCaracteresNoASCII
////
////   Description :
////
////   Parametros  :
////   Returns     :
////
////   Autor       : AGS
////   Fecha       : Mayo/2016
////===========================================================================================================
//UINT32 MathLib_RangoAleatorio(UINT32 u32Menor, UINT32 u32Mayor)
//{
//    return (rand() % (u32Mayor-u32Menor) + u32Menor);
//
//    //(N-M+1) + M
//
//}
//
////===========================================================================================================
////  Funcion   : MathLib_BuscarString();
////
////  Descripcion : Busca una cadena de caracteres dentro de una de tamano mayor, si la cadena de caracteres a
////          buscar es encontrada se retorna un puntero a la direccion de memoria siguiente de la
////          ultima coincidencia encontrada, si el proceso falla retorna un valor nulo
////
////  Parametros  : *pu8StringFuente, *pu8StringClave, *pu16Index
////
////  Returns   : SUCCESS/FAIL
////
////  Autor   : EDVS
////
////  Fecha   : 22/Julio/2016
////===========================================================================================================
//UINT8 MathLib_BuscarString (UINT8 *pu8StringFuente, UINT8 *pu8StringClave, UINT32 u32Index)
//{
//UINT16 *pu16Aux;
//UINT8 u8VarControl = 0, *pu8DirAnt;
//UINT16 u16Index = 0, u16IndexAnt;
//
//  while(*pu8StringFuente != 0)
//  {
//    RESET_WDOG;
//    if(*pu8StringClave == 0) return (FAIL);
//    if(*pu8StringFuente == *pu8StringClave)
//    {
//      u16IndexAnt = u16Index;
//      pu8DirAnt = pu8StringClave;
//      while(*pu8StringFuente == *pu8StringClave)
//      {
//        u16Index++;
//        pu8StringFuente++;
//        pu8StringClave++;
//        if(*pu8StringClave == 0)
//        {
//          pu16Aux = (UINT16*)u32Index;
//          *pu16Aux = u16IndexAnt;
//          return (SUCCESS);
//        }
//        if(*pu8StringFuente == 0) return (FAIL);
//      }
//      pu8StringClave = pu8DirAnt;
//    }
//    else
//    {
//      u16Index++;
//      pu8StringFuente++;
//    }
//  }
//  return (FAIL);
//}
//
////===========================================================================================================
////  Funcion   : MathLib_SustituirCaracterPorNull();
////
////  Descripcion :
////
////  Parametros  : u8VariableDeControl, pu8StringFuente
////
////  Returns   : SUCCESS/FAIL
////
////  Autor   : EDVS
////
////  Fecha   : 22/Julio/2016
////===========================================================================================================
//UINT8 MathLib_SustituirCaracterPorNull (UINT8 *pu8StringFuente, UINT8 u8VariableDeControl)
//{
//  while(*pu8StringFuente)
//  {
//    RESET_WDOG;
//    if(*pu8StringFuente == u8VariableDeControl)
//    {
//      *pu8StringFuente = '\0' ;
//      return (SUCCESS);
//    }
//    else pu8StringFuente ++ ;
//  }
//  return (FAIL);
//}
//
////===========================================================================================================
////  Funcion   : MathLib_ObtenerNuevaLinea();
////
////  Descripcion :
////
////  Parametros  : *pu8StringFuente, *pu8StringDestino, u8MaxCopy
////
////  Returns   : SUCCESS/FAIL
////
////  Autor   : EDVS
////
////  Fecha   : 22/Julio/2016
////===========================================================================================================
//UINT8 MathLib_ObtenerNuevaLinea (UINT8 *pu8StringFuente, UINT8 *pu8StringDestino, UINT8 u8MaxCopy)
//{
//  while((*pu8StringFuente) && (u8MaxCopy))
//  {
//    if(*pu8StringFuente == '\n')
//    {
//      *pu8StringDestino = '\0' ;
//      return (SUCCESS);
//    }
//    else
//    {
//      *pu8StringDestino = *pu8StringFuente;
//      u8MaxCopy--;
//      pu8StringDestino++;
//      pu8StringFuente++;
//    }
//    RESET_WDOG;
//  }
//  return (FAIL);
//}
//
////===========================================================================================================
////  Funcion   : MathLib_NuevaLinea();
////
////  Descripcion :
////
////  Parametros  : *pu8StringFuente, *u16SizeOfLine, u16MaxCopy
////
////  Returns   : SUCCESS/FAIL
////
////  Autor   : EDVS
////
////  Fecha   : 22/Julio/2016
////===========================================================================================================
//UINT8 MathLib_NuevaLinea (UINT8 *pu8StringFuente, UINT16 u16MaxCopy, UINT16 *u16SizeOfLine)
//{
//UINT16 u16Aux;
//
//  u16Aux = 0;
//
//  while((*pu8StringFuente) && (u16MaxCopy))
//  {
//    if(*pu8StringFuente == '\n')
//    {
//      u16Aux++;
//      *u16SizeOfLine = u16Aux;
//      return (SUCCESS);
//    }
//    else
//    {
//      u16MaxCopy--;
//      u16Aux++;
//      pu8StringFuente++;
//    }
//    RESET_WDOG;
//  }
//  return (FAIL);
//}
//
////===========================================================================================================
////  Funcion     : MathLib_ScanDecimal32uNumber();
////
////  Descripcion :
////
////  Parametros  : *pu8StringFuente, *u16SizeOfLine, u16MaxCopy
////
////  Returns     : SUCCESS/FAIL
////
////  Autor       : EDVS
////
////  Fecha       : 22/Mayo/2017
////===========================================================================================================
//UINT8 MathLib_ScanDecimal32uNumber (UINT8 *pu8String, UINT32 *pu32Valor)
//{
//  /* scans a decimal number, and stops at any non-number. Number can have any preceding zeros or spaces. */
//#define _32_NOF_DIGITS  (10+1)
//UINT8 u8MaxDigitos = _32_NOF_DIGITS; /* maximum number of digits to avoid overflow */
//UINT8 *pu8CopiaString;
//UINT32 u32Valor;
//
//  pu8CopiaString = pu8String;
//  /* skip leading spaces */
//  while(*pu8CopiaString==' ') pu8CopiaString++;
//  u32Valor = 0;
//  while(*pu8CopiaString>='0' && *pu8CopiaString<='9' && u8MaxDigitos > 0)
//  {
//    u32Valor = (UINT32)((u32Valor)*10 + *pu8CopiaString-'0');
//    u8MaxDigitos--;
//    pu8CopiaString++;
//  }
//  if(u8MaxDigitos == 0) return (FAIL);
//  /* no digits at all? */
//  if(u8MaxDigitos ==_32_NOF_DIGITS) return (FAIL);
//  *pu32Valor = u32Valor;
//  return (SUCCESS);
//}
//
////===========================================================================================================
////   Funcion     : MathLib_BuscarPalabraEnString
////
////   Description : Busca una palabra dentro del string
////   Parametros  : pu8Buffer : Apuntador a cadena a buscar
////                 pu8Palabra : Apuntador a cadena que se quiere buscar
////
////   Returns     :
////
////	 Autor       : Pedro Antonio Illan Gonzalez
////	 Fecha       : julio/2016
////===========================================================================================================
//UINT8           MathLib_BuscarPalabraEnString       (UINT8 *pu8Buffer,UINT8* pu8Palabra)
//{
//	UINT32 u32IteradorBuffer = 0;
//	UINT32 u32IteradorPalabra = 0;
//	while(pu8Buffer[u32IteradorBuffer] != 0)
//	{
//		if(pu8Buffer[u32IteradorBuffer] == pu8Palabra[u32IteradorPalabra])
//		{
//			u32IteradorPalabra++;
//			if(pu8Palabra[ u32IteradorPalabra] == 0)
//			return (SUCCESS);
// 		}
//		else
//		{
//			u32IteradorPalabra = 0;
//		}
//		u32IteradorBuffer++;
//		RESET_WDOG;
//	}
//	return (FAIL);
//}

