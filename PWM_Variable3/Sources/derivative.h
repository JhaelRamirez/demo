/*
#############################################################################################################

  Archivo      : derivative.h
  Proyecto     : Modulo Interfaz K
  Fecha Inicio : 02/Marzo/2017


=============================================================================================================

  (c) Copyright SICOM, Sistemas y Controles MINED S.A. de C.V.

  SICOM, Sistemas y Controles MINED S.A. de C.V.

  http    : www.sicom.mx
  mail    : info@sicom.com

#############################################################################################################

 derivative

#############################################################################################################
*/
#ifndef INCLUDES_DERIVATIVE_H_
#define INCLUDES_DERIVATIVE_H_

//----------------------------------------------------------------------------
//
// Lista de MCU
//
//----------------------------------------------------------------------------

//#define MK10F12
#define MK22F51212

#ifdef MK10F12

#include "MK10F12.h"
#include "core_cm4.h"
#include "DefinicionesHW_MK10.h"

#endif

#ifdef MK22F51212

#include "MK22F51212.h"
#include "core_cm4.h"

#endif

#endif /* INCLUDES_DERIVATIVE_H_ */
