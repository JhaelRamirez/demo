/*
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "MK22F51212.h"
#include "FTM.h"
#include "LED.h"
#include "GPIO.H"


int main(void)
{
	//Inicializa_Led(Led_Heart);



    for(;;)
    {

    	static UINT8 MaquinaEstadosFTM = Estado1FTM;
    	if(MaquinaEstadosFTM == Estado1FTM)
    	{
        	Frecuencia      = 100;
        	CicloDeTrabajo  = 10;
        	Frecuencia2     = 100;
        	CicloDeTrabajo2 = 10;
        	selector1       = MicroS;
        	selector2       = MicroS;

    		MaquinaEstadosFTM = Estado2FTM;
    	}
    	if(MaquinaEstadosFTM == Estado2FTM)
    	{
    	    FTM_PWM_Alineado_A_Los_Costados(Frecuencia, CicloDeTrabajo, selector1, ModuloFTM0,Canal2FTM );
            MaquinaEstadosFTM = Estado3FTM;
    	}

    	if(MaquinaEstadosFTM == Estado3FTM)
    	{
    		if((CicloDeTrabajo != CicloDeTrabajo2) | (Frecuencia != Frecuencia2) | (selector1 != selector2))
    		{
    			CicloDeTrabajo = CicloDeTrabajo2;
    			Frecuencia     = Frecuencia2;
    			selector1      = selector2;
    			MaquinaEstadosFTM = Estado2FTM;
    			FTM0_SC    = 0;
    		}

    	}

    }
    return 0;
}
