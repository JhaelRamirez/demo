/*
 * FTM.h
 *
 *  Created on: 25/09/2019
 *      Author: JRamirez
 */
#include "MK22F51212.h"
#include "Types.h"

#ifndef SOURCES_FTM_H_
#define SOURCES_FTM_H_

//----------------------------------------------------------------------------
//
// Prototipos de funciones
//
//----------------------------------------------------------------------------
void FTM_PWM_Alineado_A_Los_Costados(UINT16 frecuencia, UINT16 AnchoPulso, UINT32 UnidadDeTiempo,UINT8 ModuloFTM,UINT8 CanalEntrada);
UINT16 Calcula_Cuentas_Por_Tiempo(UINT32 UnidadDeTiempo, UINT8 Prescaler);
UINT32  Selecciona_Prescaler(UINT32 UnidadDeTiempo);

//----------------------------------------------------------------------------
//
// Unidades de tiempo
//
//----------------------------------------------------------------------------
#define MiliS        1000    //Solo entre 1 y 3400 unidades (1ms a 400ms)
#define MicroS       1000000 //Solo entre 1 y 3100 unidades (1us a 3ms)
unsigned char v1;


//----------------------------------------------------------------------------
//
// Variables
//
//----------------------------------------------------------------------------
UINT16 TiempoEnCuentas;
UINT8  UnivPrescaler;
UINT16 Frecuencia;
UINT16 CicloDeTrabajo;
UINT16 Frecuencia2;
UINT16 CicloDeTrabajo2;
UINT32 selector1;
UINT32 selector2;
typedef enum
{
  Canal0FTM = 0,
  Canal1FTM,
  Canal2FTM,
  Canal3FTM,
  Canal4FTM,
  Canal5FTM,
}CanalesFTM;

typedef enum
{
  ModuloFTM0 = 0,
  ModuloFTM1,
  ModuloFTM2,
}ModulosFTM;

typedef enum
{
  Estado1FTM = 0,
  Estado2FTM,
  Estado3FTM,
}MaquinaEstadosFTM;

#endif /* SOURCES_FTM_H_ */
