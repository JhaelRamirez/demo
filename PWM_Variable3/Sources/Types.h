/*
#############################################################################################################

  Proyecto     : Modulo Interfaz K
  Archivo      : Types.h
  Fecha Inicio : 02/Marzo/2017


=============================================================================================================

  (c) Copyright SICOM, Sistemas y Controles MINED S.A. de C.V.

  SICOM, Sistemas y Controles MINED S.A. de C.V.

  http    : www.sicom.mx
  mail    : info@sicom.com

#############################################################################################################

 Types

#############################################################################################################
*/

#ifndef TYPES_H_
#define TYPES_H_

#include "stddef.h"
//----------------------------------------------------------------------------
//
//  Direcciones posicion de los bits
//
//----------------------------------------------------------------------------

#ifndef BIT0
  #define BIT0      0x00000001                                  // Posicion logica de los BITs
  #define BIT1      0x00000002                                  // Posicion logica de los BITs
  #define BIT2      0x00000004                                  // Posicion logica de los BITs
  #define BIT3      0x00000008                                  // Posicion logica de los BITs
  #define BIT4      0x00000010                                  // Posicion logica de los BITs
  #define BIT5      0x00000020                                  // Posicion logica de los BITs
  #define BIT6      0x00000040                                  // Posicion logica de los BITs
  #define BIT7      0x00000080                                  // Posicion logica de los BITs

  #define BIT8      0x00000100                                  // Posicion logica de los BITs
  #define BIT9      0x00000200                                  // Posicion logica de los BITs
  #define BIT10     0x00000400                                  // Posicion logica de los BITs
  #define BIT11     0x00000800                                  // Posicion logica de los BITs
  #define BIT12     0x00001000                                  // Posicion logica de los BITs
  #define BIT13     0x00002000                                  // Posicion logica de los BITs
  #define BIT14     0x00004000                                  // Posicion logica de los BITs
  #define BIT15     0x00008000                                  // Posicion logica de los BITs

  #define BIT16     0x00010000                                  // Posicion logica de los BITs
  #define BIT17     0x00020000                                  // Posicion logica de los BITs
  #define BIT18     0x00040000                                  // Posicion logica de los BITs
  #define BIT19     0x00080000                                  // Posicion logica de los BITs
  #define BIT20     0x00100000                                  // Posicion logica de los BITs
  #define BIT21     0x00200000                                  // Posicion logica de los BITs
  #define BIT22     0x00400000                                  // Posicion logica de los BITs
  #define BIT23     0x00800000                                  // Posicion logica de los BITs

  #define BIT24     0x01000000                                  // Posicion logica de los BITs
  #define BIT25     0x02000000                                  // Posicion logica de los BITs
  #define BIT26     0x04000000                                  // Posicion logica de los BITs
  #define BIT27     0x08000000                                  // Posicion logica de los BITs
  #define BIT28     0x10000000                                  // Posicion logica de los BITs
  #define BIT29     0x20000000                                  // Posicion logica de los BITs
  #define BIT30     0x40000000                                  // Posicion logica de los BITs
  #define BIT31     0x80000000                                  // Posicion logica de los BITs

#endif

/*
 * Standard Definitions:
 * These defines allow for easier porting to other compilers. if porting change
 * these defines to the required values for the chosen compiler.
 */

//----------------------------------------------------------------------------
//
//  Tipos de datos
//
//----------------------------------------------------------------------------
typedef unsigned char   UINT8;      // unsigned 8 bit definition 
typedef unsigned short  UINT16;     // unsigned 16 bit definition
typedef unsigned long   UINT32;     // unsigned 32 bit definition
typedef signed char     INT8;       // signed 8 bit definition 
typedef short           INT16;      // signed 16 bit definition
typedef long int        INT32;      // signed 32 bit definition

#define TRUE      1    //!< True
#define FALSE     0    //!< False

#define FAIL      1    //!< Falla
#define SUCCESS   0    //!< Exito
#define WORKING   2    //!< Trabajando (para maquinas de estado)

typedef enum
{
  eP_Procesando = 0,
  eP_Success,
  eP_Fail,
}eProceso;

#endif /* TYPES_H_ */
