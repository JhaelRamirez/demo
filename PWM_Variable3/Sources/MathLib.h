#ifndef _MathLib_H_
#define _MathLib_H_

#include "Types.h" 

//----------------------------------------------------------------------------
// 
//Prototipo de funciones
//
//----------------------------------------------------------------------------

UINT8        MathLib_ByteBcdToHex                 (UINT8 B);
UINT8        MathLib_ByteHexToBcd                 (UINT8 B);
UINT8        MathLib_CharBcdToHex                 (UINT8 B);
void         MathLib_BcdToHex                     (UINT8 *Ptr, UINT8 Size);
void         MathLib_HexToBcd                     (UINT8 *Ptr, UINT8 Size);

UINT8        MathLib_HexAsc1                      (UINT8 B);
UINT16       MathLib_HexAsc2                      (UINT8 B);

void         MathLib_HexToAsc                     (UINT8 *String, UINT8 *Dato, UINT8 Size);
void         MathLib_CharToAsc                    (UINT8 *String, UINT8 cDato);
void         MathLib_IntToAsc                     (UINT8 *String, UINT16  iDato);
void         MathLib_LongToAsc                    (UINT8 *String, UINT32 lDato);
void         MathLib_FloatToAsc                   (UINT8 *String, float fDato);


UINT8        MathLib_AscToByte                    (UINT8 Dato);
UINT8        MathLib_AscToChar                    (UINT8 *String);
UINT16       MathLib_AscToInt                     (UINT8 *String);
UINT32       MathLib_AscToLong                    (UINT8 *String);
float        MathLib_AscToFloat                   (UINT8 *String);
double       MathLib_AscToDouble                  (UINT8 *String);
UINT8        MathLib_AscToBcd                     (UINT8 *String);
void         MathLib_AscToHex                     (UINT8 *PtrHex, UINT8 *PtrAsc, UINT8 Size);

float        MathLib_RoundFloat                   (float fNum, UINT8 u8Decimales);
double       MathLib_RoundDouble                  (double dNum, UINT8 u8Decimales);

UINT32       MathLib_Residuo                      (UINT32 LDato, UINT32 LDiv);
UINT8        MathLib_SumaBcdChar                  (UINT8 Dato1, UINT8 Dato2);
UINT8        MathLib_RestaBcdChar                 (UINT8 Dato1, UINT8 Dato2);

void         MathLib_StringClear                  (UINT8 *str);
UINT16       MathLib_StringCopy                   (UINT8 *str_destino, UINT8 *str_fuente);
void         MathLib_StringCopyMax                (UINT8 *str_destino, UINT8 *str_fuente, UINT32 Count);
void         MathLib_StringNCopy                  (UINT8 *str_destino, UINT8 *str_fuente, UINT32 Count);

void         MathLib_StringCopyMaxEndNULL         (UINT8 *str_destino, UINT8 *str_fuente, UINT32 Count);
void         MathLib_ConcatenarStrings            (UINT8* str_destino ,UINT8 *str_fuente);
void         MathLib_SustituirNCaracterEnStrings  (UINT8* str ,UINT8 u8CaracterBuscado,UINT8 u8NuevoCaracter,UINT32 u32NumeroDeCaracteres);
UINT32       MathLib_BitMask                      (UINT32 Bit);
UINT32       MathLib_BitMaskNot                   (UINT32 Bit);
UINT8        MathLib_BitMaskOff                   (UINT8 Bit);
void         Mathlib_InicializarAleatorios        (unsigned int u16Semilla);
void         MathLib_ConvierteABaseSuperior       (UINT8 *pu8NumeroOriginal, UINT8 u8Base, UINT8 *pu8NumeroConvertido, UINT8 *pu8Cociente, UINT8 *pu8Dividendo, UINT8 *pu8Aux);

void         MathLib_StringConcatenaNibbles       (UINT8 *pu8String);
UINT8        MathLib_LittleEndianToBigEndian      (UINT8 *Ptr, UINT8 Size, UINT8 *Result);
void         MathLib_LittleEndianToBigEndianData  (UINT8 *pDato, UINT8 Size);

void         MathLib_CastData                     (UINT32 Address, UINT8 u8Size, UINT8 *pu8Data);

void         MathLib_QuitarCaracteresDeControl    (UINT8 *Str) ;
UINT32       MathLib_RangoAleatorio               (UINT32 u32Menor, UINT32 u32Mayor);

UINT8        MathLib_BuscarString                 (UINT8 *pu8StringFuente, UINT8 *pu8StringClave, UINT32 u32Index);
UINT8        MathLib_SustituirCaracterPorNull     (UINT8 *pu8StringFuente, UINT8 u8VariableDeControl);
UINT8        MathLib_ObtenerNuevaLinea            (UINT8 *pu8StringFuente, UINT8 *pu8StringDestino, UINT8 u8MaxCopy);
UINT8        MathLib_NuevaLinea                   (UINT8 *pu8StringFuente, UINT16 u16MaxCopy, UINT16 *u16SizeOfLine);
UINT8        MathLib_ScanDecimal32uNumber         (UINT8 *pu8String, UINT32 *pu32Valor);
UINT8           MathLib_BuscarPalabraEnString       			(UINT8 *pu8Buffer,UINT8* pu8Palabra);

#endif
