/*
 * LES.c
 *
 *  Created on: 26/09/2019
 *      Author: JRamirez
 */

#include "LED.h"
#include "GPIO.h"

/********************************************************************************************************************************
 *Name:          Inicializa_Led
 *
 *Definicion:    Usa la biblioteca GPIO para configurar un led (Seleccionado con el parametro de entrada Led_numeroU8)
 *               Alguno de los modos de MUX.
 *
 *Entradas:      Led_numeroU8, Led_funcion_MUXU8.
 *
 *Salidas:       Ninguna.
 ********************************************************************************************************************************/
void Inicializa_Led(UINT8 Selecciona_Led)
{
	GPIO_InicializarBit ( edpm_Salida, epm_PuertoB, Selecciona_Led);
}


/********************************************************************************************************************************
 *Name:          Led_Toggle_Estado_Acutal
 *
 *Definicion:    Hace toggle al estado Actual del led seleccionado mediante el parametro de entrada "Led_a_TogglearU8"
 *
 *Entradas:      Led_a_TogglearU8.
 *
 *Salidas:       Ninguna.
 ********************************************************************************************************************************/
void Led_Toggle_Estado_Acutal(UINT8 Selecciona_Led)
{
	GPIO_ToggleOutputStatus (epm_PuertoB,Selecciona_Led);
}

