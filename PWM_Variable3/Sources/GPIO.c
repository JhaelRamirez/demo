/*
#############################################################################################################

  Archivo      : GPIO.c
  Proyecto     : Modulo Interfaz K
  Fecha Inicio : 01/Noviembre/2014


=============================================================================================================

  (c) Copyright SICOM, Sistemas y Controles MINED S.A. de C.V.

  SICOM, Sistemas y Controles MINED S.A. de C.V.

  http    : www.sicom.mx
  mail    : info@sicom.com

#############################################################################################################

 GPIO

 Esta libreria es para el uso  puertos de entrada y salida

#############################################################################################################
*/

#include "derivative.h"

#include "MathLib.h"
#include "GPIO.h"

//===========================================================================================================
/** \brief Funtion: GPIO_InicializarBit().

    \details Description:inicializa puetros A, B, C, D, E

    \param   eDireccionPuertosMicro Direccion,   ePuertosMicro Puerto, eBitsPuertoMicro Bit

    \return  Nothig

    \author  Pedro Antonio Illan Gonzalez

    \date    05/Julio/2019
**/
//===========================================================================================================
UINT8  GPIO_InicializarBit ( eDireccionPuertosMicro Direccion, ePuertosMicro Puerto, eBitsPuertoMicro Bit)
{
UINT32 U32Aux;

  if(Direccion  > edpm_Ultimo) return(FAIL);//Fail
  if(Puerto  > emp_Ultimo) return(FAIL); //Fail
  if(Bit  > ebpm_Ultimo) return(FAIL); //Fail


  switch(Puerto)
  {
    case epm_PuertoA:
      GPIO_SELECTOR_RELOJ |= GPIO_PORTA_MASK;
      U32Aux = PORT_PCR_REG(PORTA , Bit);
      PORT_PCR_REG(PORTA , Bit) = PORT_PCR_MUX(1); //PORTx_PCRn  //aignar multiplexor
      PORT_PCR_REG(PORTA , Bit) |= (U32Aux & ~PORT_PCR_MUX(7));
      if(Direccion == 1)   GPIOA_PDDR |= (1 << Bit); // direccion del puerto
      else GPIOA_PDDR &= MathLib_BitMaskNot(Bit);
      break;

    case epm_PuertoB:
      GPIO_SELECTOR_RELOJ |= GPIO_PORTB_MASK;
      U32Aux = PORT_PCR_REG(PORTB , Bit);
      PORT_PCR_REG(PORTB , Bit) = PORT_PCR_MUX(1); //PORTx_PCRn  //aignar multiplexor
      PORT_PCR_REG(PORTB , Bit) |= (U32Aux & ~PORT_PCR_MUX(7));
      if(Direccion == 1)   GPIOB_PDDR |= (1 << Bit);
      else GPIOB_PDDR &= MathLib_BitMaskNot(Bit);
      break;

    case epm_PuertoC:
      GPIO_SELECTOR_RELOJ |= GPIO_PORTC_MASK;
      U32Aux = PORT_PCR_REG(PORTC , Bit);
      PORT_PCR_REG(PORTC , Bit) = PORT_PCR_MUX(1); //PORTx_PCRn  //aignar multiplexor
      PORT_PCR_REG(PORTC , Bit) |= (U32Aux & ~PORT_PCR_MUX(7));
      if(Direccion == 1)   GPIOC_PDDR |= (1 << Bit);
      else  GPIOC_PDDR  &= MathLib_BitMaskNot(Bit);
      break;

    case epm_PuertoD:
      GPIO_SELECTOR_RELOJ |= GPIO_PORTD_MASK;
      U32Aux = PORT_PCR_REG(PORTD , Bit);
      PORT_PCR_REG(PORTD , Bit) = PORT_PCR_MUX(1); //PORTx_PCRn  //aignar multiplexor
      PORT_PCR_REG(PORTD , Bit) |= (U32Aux & ~PORT_PCR_MUX(7));
      if(Direccion == 1)  GPIOD_PDDR |= (1 << Bit);
      else GPIOD_PDDR  &= MathLib_BitMaskNot(Bit);
      break;

    case epm_PuertoE:
      GPIO_SELECTOR_RELOJ |= GPIO_PORTE_MASK;
      U32Aux = PORT_PCR_REG(PORTE , Bit);
      PORT_PCR_REG(PORTE , Bit) = PORT_PCR_MUX(1); //PORTx_PCRn  //aignar multiplexor
      PORT_PCR_REG(PORTE , Bit) |= (U32Aux & ~PORT_PCR_MUX(7));
      if(Direccion == edpm_Salida)   GPIOE_PDDR |= (1 << Bit);
      else GPIOE_PDDR  &= MathLib_BitMaskNot(Bit);
      break;

    case emp_Ultimo:
    default:
      return(FAIL); //Fail
      break;
  }

  return(SUCCESS); // Success
}
//===========================================================================================================
/** \brief Funtion: GPIO_PullUp().

    \details Description: Habilita o deshabilita la resistencia del pull up del pin seleccionado

    \param   eActivar, Puerto, Bit

    \return  Nothig

    \author  Pedro Antonio Illan Gonzalez

    \date    05/Julio/2019
**/
//===========================================================================================================
UINT8 GPIO_PullUp (eEstadoPullUp eActivar, ePuertosMicro Puerto, eBitsPuertoMicro Bit)
{
  if(Puerto  > emp_Ultimo) return(FAIL); //Fail
  if(Bit  > ebpm_Ultimo) return(FAIL); //Fail

  switch(Puerto)
  {
    case epm_PuertoA:
      if(eActivar == 1)
      {
        PORT_PCR_REG(PORTA , Bit) |= (PORT_PCR_PS_MASK | PORT_PCR_PE_MASK);
        if(PORT_PCR_REG(PORTA , Bit) & PORT_PCR_PS_MASK) return (SUCCESS);
        else return (FAIL);
      }
      else
      {
        PORT_PCR_REG(PORTA , Bit) &= ~PORT_PCR_PE_MASK;
        if(PORT_PCR_REG(PORTA , Bit) & PORT_PCR_PS_MASK) return (FAIL);
        else return (SUCCESS);
      }
      break;

    case epm_PuertoB:
      if(eActivar == 1)
      {
        PORT_PCR_REG(PORTB , Bit) |= (PORT_PCR_PS_MASK | PORT_PCR_PE_MASK);
        if(PORT_PCR_REG(PORTB , Bit) & PORT_PCR_PS_MASK) return (SUCCESS);
        else return (FAIL);
      }
      else
      {
        PORT_PCR_REG(PORTB , Bit) &= ~PORT_PCR_PE_MASK;
        if(PORT_PCR_REG(PORTB , Bit) & PORT_PCR_PS_MASK) return (FAIL);
        else return (SUCCESS);
      }
      break;

    case epm_PuertoC:
      if(eActivar == 1)
      {
        PORT_PCR_REG(PORTC , Bit) |= (PORT_PCR_PS_MASK | PORT_PCR_PE_MASK);
        if(PORT_PCR_REG(PORTC , Bit) & PORT_PCR_PS_MASK) return (SUCCESS);
        else return (FAIL);
      }
      else
      {
        PORT_PCR_REG(PORTC , Bit) &= ~PORT_PCR_PE_MASK;
        if(PORT_PCR_REG(PORTC , Bit) & PORT_PCR_PS_MASK) return (FAIL);
        else return (SUCCESS);
      }
      break;

    case epm_PuertoD:
      if(eActivar == 1)
      {
        PORT_PCR_REG(PORTD , Bit) |= (PORT_PCR_PS_MASK | PORT_PCR_PE_MASK);
        if(PORT_PCR_REG(PORTD , Bit) & PORT_PCR_PS_MASK) return (SUCCESS);
        else return (FAIL);
      }
      else
      {
        PORT_PCR_REG(PORTD , Bit) &= ~PORT_PCR_PE_MASK;
        if(PORT_PCR_REG(PORTD , Bit) & PORT_PCR_PS_MASK) return (FAIL);
        else return (SUCCESS);
      }
      break;

    case epm_PuertoE:
      if(eActivar == 1)
      {
        PORT_PCR_REG(PORTE , Bit) |= (PORT_PCR_PS_MASK | PORT_PCR_PE_MASK);
        if(PORT_PCR_REG(PORTE , Bit) & PORT_PCR_PS_MASK) return (SUCCESS);
        else return (FAIL);
      }
      else
      {
        PORT_PCR_REG(PORTE , Bit) &= ~PORT_PCR_PE_MASK;
        if(PORT_PCR_REG(PORTE , Bit) & PORT_PCR_PS_MASK) return (FAIL);
        else return (SUCCESS);
      }
      break;

    case emp_Ultimo:
    default:
      return(FAIL); //Fail
      break;
  }

  return(SUCCESS); // Success
}
//===========================================================================================================
/** \brief Funtion: GPIO_PullDown().

    \details Description: Habilita o deshabilita la resistencia del pull down del pin seleccionado

    \param   eActivar, Puerto, Bit

    \return  Nothig

    \author  Pedro Antonio Illan Gonzalez

    \date    05/Julio/2019
**/
//===========================================================================================================
UINT8 GPIO_PullDown (eEstadoPullUp eActivar, ePuertosMicro Puerto, eBitsPuertoMicro Bit)
{
  if(Puerto  > emp_Ultimo) return(FAIL); //Fail
  if(Bit  > ebpm_Ultimo) return(FAIL); //Fail

  switch(Puerto)
  {
    case epm_PuertoA:
      if(eActivar == 1)
      {
        PORT_PCR_REG(PORTA , Bit) |= PORT_PCR_PE_MASK;
        PORT_PCR_REG(PORTA , Bit) &= ~PORT_PCR_PS_MASK;
        if(PORT_PCR_REG(PORTA , Bit) & PORT_PCR_PS_MASK) return (SUCCESS);
        else return (FAIL);
      }
      else
      {
        PORT_PCR_REG(PORTA , Bit) &= ~PORT_PCR_PE_MASK;
        if(PORT_PCR_REG(PORTA , Bit) & PORT_PCR_PS_MASK) return (FAIL);
        else return (SUCCESS);
      }
      break;

    case epm_PuertoB:
      if(eActivar == 1)
      {
        PORT_PCR_REG(PORTB , Bit) |= PORT_PCR_PE_MASK;
        PORT_PCR_REG(PORTB , Bit) &= ~PORT_PCR_PS_MASK;
        if(PORT_PCR_REG(PORTB , Bit) & PORT_PCR_PS_MASK) return (SUCCESS);
        else return (FAIL);
      }
      else
      {
        PORT_PCR_REG(PORTB , Bit) &= ~PORT_PCR_PE_MASK;
        if(PORT_PCR_REG(PORTB , Bit) & PORT_PCR_PS_MASK) return (FAIL);
        else return (SUCCESS);
      }
      break;

    case epm_PuertoC:
      if(eActivar == 1)
      {
        PORT_PCR_REG(PORTC , Bit) |= PORT_PCR_PE_MASK;
        PORT_PCR_REG(PORTC , Bit) &= ~PORT_PCR_PS_MASK;
        if(PORT_PCR_REG(PORTC , Bit) & PORT_PCR_PS_MASK) return (SUCCESS);
        else return (FAIL);
      }
      else
      {
        PORT_PCR_REG(PORTC , Bit) &= ~PORT_PCR_PE_MASK;
        if(PORT_PCR_REG(PORTC , Bit) & PORT_PCR_PS_MASK) return (FAIL);
        else return (SUCCESS);
      }
      break;

    case epm_PuertoD:
      if(eActivar == 1)
      {
        PORT_PCR_REG(PORTD , Bit) |= PORT_PCR_PE_MASK;
        PORT_PCR_REG(PORTD , Bit) &= ~PORT_PCR_PS_MASK;
        if(PORT_PCR_REG(PORTD , Bit) & PORT_PCR_PS_MASK) return (SUCCESS);
        else return (FAIL);
      }
      else
      {
        PORT_PCR_REG(PORTD , Bit) &= ~PORT_PCR_PE_MASK;
        if(PORT_PCR_REG(PORTD , Bit) & PORT_PCR_PS_MASK) return (FAIL);
        else return (SUCCESS);
      }
      break;

    case epm_PuertoE:
      if(eActivar == 1)
      {
        PORT_PCR_REG(PORTE , Bit) |= PORT_PCR_PE_MASK;
        PORT_PCR_REG(PORTE , Bit) &= ~PORT_PCR_PS_MASK;
        if(PORT_PCR_REG(PORTE , Bit) & PORT_PCR_PS_MASK) return (SUCCESS);
        else return (FAIL);
      }
      else
      {
        PORT_PCR_REG(PORTE , Bit) &= ~PORT_PCR_PE_MASK;
        if(PORT_PCR_REG(PORTE , Bit) & PORT_PCR_PS_MASK) return (FAIL);
        else return (SUCCESS);
      }
      break;

    case emp_Ultimo:
    default:
      return(FAIL); //Fail
      break;
  }

  return(SUCCESS); // Success
}
//===========================================================================================================
/** \brief Funtion: GPIO_AltaImpedancia().

    \details Description: Deshabilita los puertos A, B, C, D, E

    \param   eEstadoAltaImpedancia eActivar ePuertosMicro Puerto, eBitsPuertoMicro Bit

    \return  Nothig

    \author  Pedro Antonio Illan Gonzalez

    \date    05/Julio/2019
**/
//===========================================================================================================
UINT8 GPIO_AltaImpedancia (ePuertosMicro Puerto, eBitsPuertoMicro Bit)
{
  if(Puerto  > emp_Ultimo) return(FAIL); //Fail
  if(Bit  > ebpm_Ultimo) return(FAIL); //Fail


  switch(Puerto)
  {
    case epm_PuertoA:
      GPIO_SELECTOR_RELOJ |= GPIO_PORTA_MASK;
      PORT_PCR_REG(PORTA , Bit) &= ~PORT_PCR_MUX(7);
      break;

    case epm_PuertoB:
      GPIO_SELECTOR_RELOJ |= GPIO_PORTB_MASK;
      PORT_PCR_REG(PORTB , Bit) &= ~PORT_PCR_MUX(7);
      break;

    case epm_PuertoC:
      GPIO_SELECTOR_RELOJ |= GPIO_PORTC_MASK;
      PORT_PCR_REG(PORTC , Bit) &= ~PORT_PCR_MUX(7);
      break;

    case epm_PuertoD:
      GPIO_SELECTOR_RELOJ |= GPIO_PORTD_MASK;
      PORT_PCR_REG(PORTD , Bit) &= ~PORT_PCR_MUX(7);
      break;

    case epm_PuertoE:
      GPIO_SELECTOR_RELOJ |= GPIO_PORTE_MASK;
      PORT_PCR_REG(PORTE , Bit) &= ~PORT_PCR_MUX(7);
      break;

    case emp_Ultimo:
    default:
      return(FAIL); //Fail
      break;
  }

  return(SUCCESS); // Success
}

//===========================================================================================================
/** \brief Funtion: GPIO_Alt3().

    \details Description: Habilita un pin de GPIO como ALT3. A, B, C, D, E

    \param   eEstadoAltaImpedancia eActivar ePuertosMicro Puerto, eBitsPuertoMicro Bit

    \return  Nothig

    \author  Pedro Antonio Illan Gonzalez

    \date    05/Julio/2019
**/
//===========================================================================================================
UINT8 GPIO_Alt3(ePuertosMicro Puerto, eBitsPuertoMicro Bit)
{
  if(Puerto  > emp_Ultimo) return(FAIL); //Fail
  if(Bit  > ebpm_Ultimo) return(FAIL); //Fail


  switch(Puerto)
  {
    case epm_PuertoA:
      GPIO_SELECTOR_RELOJ |= GPIO_PORTA_MASK;
      PORT_PCR_REG(PORTA , Bit) = PORT_PCR_MUX(3);
      break;

    case epm_PuertoB:
      GPIO_SELECTOR_RELOJ |= GPIO_PORTB_MASK;
      PORT_PCR_REG(PORTB , Bit) = PORT_PCR_MUX(3);
      break;

    case epm_PuertoC:
      GPIO_SELECTOR_RELOJ |= GPIO_PORTC_MASK;
      PORT_PCR_REG(PORTC , Bit) = PORT_PCR_MUX(3);
      break;

    case epm_PuertoD:
      GPIO_SELECTOR_RELOJ |= GPIO_PORTD_MASK;
      PORT_PCR_REG(PORTD , Bit) = PORT_PCR_MUX(3);
      break;

    case epm_PuertoE:
      GPIO_SELECTOR_RELOJ |= GPIO_PORTE_MASK;
      PORT_PCR_REG(PORTE , Bit) = PORT_PCR_MUX(3);
      break;

    case emp_Ultimo:
    default:
      return(FAIL); //Fail
      break;
  }

  return(SUCCESS); // Success
}
//===========================================================================================================
/** \brief Funtion: GPIO_SetBitStatus().

    \details Description:  pone el estado seleccionado el bit del puerto elegido

    \param  ePuertosMicro Puerto, eBitsPuertoMicro Bit, eEstadoBit Estado

    \return  Nothig

    \author  Pedro Antonio Illan Gonzalez

    \date    05/Julio/2019
**/
//===========================================================================================================
void  GPIO_SetBitStatus (ePuertosMicro Puerto, eBitsPuertoMicro Bit, eEstadoBit Estado)
{
  switch(Puerto)
  {
    case epm_PuertoA:
      if(Estado) GPIOA_PDOR |= MathLib_BitMask(Bit);
      else GPIOA_PDOR  &= MathLib_BitMaskNot(Bit);
      break;

    case epm_PuertoB:
      if(Estado) GPIOB_PDOR |= MathLib_BitMask(Bit);
      else GPIOB_PDOR  &= MathLib_BitMaskNot(Bit);
      break;

    case epm_PuertoC:
      if(Estado) GPIOC_PDOR |= MathLib_BitMask(Bit);
      else GPIOC_PDOR  &= MathLib_BitMaskNot(Bit);
      break;

    case epm_PuertoD:
      if(Estado) GPIOD_PDOR |= MathLib_BitMask(Bit);
      else GPIOD_PDOR  &= MathLib_BitMaskNot(Bit);
      break;

    case epm_PuertoE:
      if(Estado) GPIOE_PDOR |= MathLib_BitMask(Bit);
      else GPIOE_PDOR  &= MathLib_BitMaskNot(Bit);
      break;

    case emp_Ultimo:
    default:
      break;
  }
}
//===========================================================================================================
/** \brief Funtion: GPIO_GetBitStatus().

    \details Description:   obtiene le estado del bit elegido

    \param  PuertosMicro Puerto, eBitsPuertoMicro Bit, eEstadoBit Estado

    \return  Nothig

    \author  Pedro Antonio Illan Gonzalez

    \date    05/Julio/2019
**/
//===========================================================================================================

UINT8  GPIO_GetBitStatus    (ePuertosMicro Puerto, eBitsPuertoMicro Bit)
{
  switch(Puerto)
  {
  case epm_PuertoA:
    if(GPIOA_PDIR &  MathLib_BitMask(Bit)) return 1;
    else return 0;
    break;

  case epm_PuertoB:
    if( GPIOB_PDIR &  MathLib_BitMask(Bit)) return 1;
    else return 0;
    break;

  case epm_PuertoC:
    if(GPIOC_PDIR &  MathLib_BitMask(Bit)) return 1;
    else return 0;
    break;

  case epm_PuertoD:
    if(GPIOD_PDIR &  MathLib_BitMask(Bit)) return 1;
    else return 0;
    break;

  case epm_PuertoE:
    if (GPIOE_PDIR &  MathLib_BitMask(Bit)) return 1;
    else return 0;
    break;
  default :
    return 0;
    break;
  }
  return 0;
}
//===========================================================================================================
/** \brief Funtion: GPIO_ToggleOutputStatus().

    \details Description:   cambia el estado del puerto y el bit seleccionados  cada que se entra a la funcion

    \param  ePuertosMicro Puerto, eBitsPuertoMicro Bit

    \return  Nothig

    \author  Pedro Antonio Illan Gonzalez

    \date    05/Julio/2019
**/
//===========================================================================================================
void  GPIO_ToggleOutputStatus (ePuertosMicro Puerto, eBitsPuertoMicro Bit)
{
  switch(Puerto)
  {
    case epm_PuertoA:
       GPIOA_PTOR |= MathLib_BitMask(Bit);
       break;

    case epm_PuertoB:
       GPIOB_PTOR |= MathLib_BitMask(Bit);
      break;

    case epm_PuertoC:
       GPIOC_PTOR |= MathLib_BitMask(Bit);
      break;

    case epm_PuertoD:
       GPIOD_PTOR |= MathLib_BitMask(Bit);
      break;

    case epm_PuertoE:
       GPIOE_PTOR |= MathLib_BitMask(Bit);
      break;

    case emp_Ultimo:
    default:
      break;
  }
}

//===========================================================================================================
/** \brief Funtion: GPIO_InicializarBit().

    \details Description:   inicializa puetros A, B, C, D, E

    \param   eDireccionPuertosMicro Direccion,   ePuertosMicro Puerto, eBitsPuertoMicro Bit

    \return  Nothig

    \author  Pedro Antonio Illan Gonzalez

    \date    05/Julio/2019
**/
//===========================================================================================================
UINT8  GPIO_ConfigurarDireccionBit ( eDireccionPuertosMicro Direccion, ePuertosMicro Puerto, eBitsPuertoMicro Bit)
{
  if(Direccion  > edpm_Ultimo) return(FAIL);//Fail
  if(Puerto  > emp_Ultimo) return(FAIL); //Fail
  if(Bit  > ebpm_Ultimo) return(FAIL); //Fail


  switch(Puerto)
  {
    case epm_PuertoA:
      if(Direccion == edpm_Salida)   GPIOA_PDDR |= (1 << Bit);     // direccion del puerto
      else GPIOA_PDDR &= MathLib_BitMaskNot(Bit);
      break;

    case epm_PuertoB:
      if(Direccion == edpm_Salida)   GPIOB_PDDR |= (1 << Bit);
      else GPIOB_PDDR &= MathLib_BitMaskNot(Bit);
      break;

    case epm_PuertoC:
      if(Direccion == edpm_Salida)   GPIOC_PDDR |= (1 << Bit);
      else  GPIOC_PDDR  &= MathLib_BitMaskNot(Bit);
      break;

    case epm_PuertoD:
      if(Direccion == edpm_Salida)  GPIOD_PDDR |= (1 << Bit);
      else GPIOD_PDDR  &= MathLib_BitMaskNot(Bit);
      break;

    case epm_PuertoE:
      if(Direccion == edpm_Salida)   GPIOE_PDDR |= (1 << Bit);
      else GPIOE_PDDR  &= MathLib_BitMaskNot(Bit);
      break;

    case emp_Ultimo:
    default:
      return(FAIL); //Fail
      break;
  }

  return(SUCCESS); // Success
}
