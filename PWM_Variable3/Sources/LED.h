/*
 * LED.h
 *
 *  Created on: 26/09/2019
 *      Author: JRamirez
 */
#include "MK22F51212.h"
#include "GPIO.h"
#include "Types.h"

#ifndef SOURCES_LED_H_
#define SOURCES_LED_H_

//----------------------------------------------------------------------------
//
// Declaraciones de funciones
//
//----------------------------------------------------------------------------
void Inicializa_Led( UINT8 Selecciona_Led);
void Led_Toggle_Estado_Acutal(UINT8 Selecciona_Led);

//----------------------------------------------------------------------------
//
// Declaraciones
//
//----------------------------------------------------------------------------
#define Led_Heart 21
#define GPIO      1


#endif /* SOURCES_LED_H_ */
