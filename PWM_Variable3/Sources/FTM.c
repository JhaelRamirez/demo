/*
 * FTM.c
 *
 *  Created on: 25/09/2019
 *      Author: JRamirez
 */
#include "FTM.h"
#include "LED.h"
#include "GPIO.h"

/********************************************************************************************************************************
 *Name:          Inicializa_FTM_Para
 *
 *Definition:    Establece los valores necesarios en los registros que hacen funcionar el modulo FTM0 en PWM
 *               alineado a los costados.
 *
 *Inputs:        CuentasMOD. Proporciona el valor en tiempo del modulo del FTM counter
 *               MilisegundosCANAL. Proporciona el valor en tiempo del Canal seleccionado
 *
 * Outputs:      None
 ********************************************************************************************************************************/
void FTM_PWM_Alineado_A_Los_Costados(UINT16 frecuencia, UINT16 AnchoPulso, UINT32 UnidadDeTiempo, UINT8 ModuloFTM,UINT8 CanalEntrada)
{
	UnivPrescaler = Selecciona_Prescaler(UnidadDeTiempo);
	TiempoEnCuentas = Calcula_Cuentas_Por_Tiempo(UnidadDeTiempo,UnivPrescaler);

	switch (ModuloFTM)
   	{
	case ModuloFTM0:
		NVIC_ClearPendingIRQ(FTM0_IRQn);
		SIM_SCGC6 |= SIM_SCGC6_FTM0(1);                             //Clock Gating para el modulo FTM0
		FTM0_SC    = 0;                                             //Apaga el modulo FTM0 antes de usarlo
		FTM0_MODE |= FTM_MODE_WPDIS(1) | FTM_MODE_INIT(1);
	    FTM0_CONF |=  FTM_CONF_BDMMODE(3);                          //FTM0 continua contando en debug mode.

	    FTM0_SC   |= FTM_SC_CPWMS(0) | FTM_SC_CLKS(1)               //CPWMS = 0, System Clock como salida (20.97152MHz)
     			  | FTM_SC_PS(UnivPrescaler) | FTM_SC_TOIE(0);      //Selecciona el prescaler basandose en  UnivPrescaler, interrupcion por overflow
	    FTM0_MOD  = (TiempoEnCuentas * (frecuencia))-1;
		switch (CanalEntrada)
		{
		case Canal0FTM:
			FTM0_C0SC |= FTM_CnSC_MSB(1) |  FTM_CnSC_MSA(0)             //MSnB:MSnA = 1:0 Para PWM alineado alos lados
					  | FTM_CnSC_ELSB(0) |  FTM_CnSC_ELSA(1)            //ELSnB:ELSnA = 0:1 Para pulsos bajos como true
					  | FTM_CnSC_CHIE(1);                               //Activa las interrupciones por evento del canal
			FTM0_C1V  |= (TiempoEnCuentas * AnchoPulso);
			GPIO_Alt3(epm_PuertoA, ebpm_Bit3);
			break;
		case Canal1FTM:
			FTM0_C1SC |= FTM_CnSC_MSB(1) |  FTM_CnSC_MSA(0)             //MSnB:MSnA = 1:0 Para PWM alineado alos lados
					  | FTM_CnSC_ELSB(0) |  FTM_CnSC_ELSA(1)            //ELSnB:ELSnA = 0:1 Para pulsos bajos como true
					  | FTM_CnSC_CHIE(1);                               //Activa las interrupciones por evento del canal
			FTM0_C1V  |= (TiempoEnCuentas * AnchoPulso);
			GPIO_Alt3(epm_PuertoA, ebpm_Bit4);
			break;
		case Canal2FTM:
			FTM0_C2SC |= FTM_CnSC_MSB(1) |  FTM_CnSC_MSA(0)             //MSnB:MSnA = 1:0 Para PWM alineado alos lados
					  | FTM_CnSC_ELSB(1) |  FTM_CnSC_ELSA(0)            //ELSnB:ELSnA = 0:1 Para pulsos bajos como true
					  | FTM_CnSC_CHIE(0);                               //Activa las interrupciones por evento del canal
			GPIO_Alt3(epm_PuertoA, ebpm_Bit5);
			FTM0_C2V  = (TiempoEnCuentas * AnchoPulso);
			break;
		}

	break;
	case ModuloFTM1:
			NVIC_ClearPendingIRQ(FTM0_IRQn);
			SIM_SCGC6 |= SIM_SCGC6_FTM1(1);                             //Clock Gating para el modulo FTM0
			FTM1_SC    = 0;                                             //Apaga el modulo FTM0 antes de usarlo
			FTM1_MODE |= FTM_MODE_WPDIS(1) | FTM_MODE_INIT(1);
			FTM1_CONF |=  FTM_CONF_BDMMODE(3);                          //FTM0 continua contando en debug mode.

			FTM1_SC   |= FTM_SC_CPWMS(0) | FTM_SC_CLKS(1)               //CPWMS = 0, System Clock como salida (20.97152MHz)
		     			  | FTM_SC_PS(UnivPrescaler) | FTM_SC_TOIE(0);      //Selecciona el prescaler basandose en  UnivPrescaler, interrupcion por overflow
			FTM1_MOD  |= (TiempoEnCuentas * (frecuencia))-1;
		switch (CanalEntrada)
		{
		case Canal0FTM:
			FTM1_C0SC |= FTM_CnSC_MSB(1) |  FTM_CnSC_MSA(0)             //MSnB:MSnA = 1:0 Para PWM alineado alos lados
				  | FTM_CnSC_ELSB(0) |  FTM_CnSC_ELSA(1)            //ELSnB:ELSnA = 0:1 Para pulsos bajos como true
					  | FTM_CnSC_CHIE(0);                               //Activa las interrupciones por evento del canal
			FTM1_C1V  |= (TiempoEnCuentas * AnchoPulso);
			break;
		case Canal1FTM:
			FTM1_C1SC |= FTM_CnSC_MSB(1) |  FTM_CnSC_MSA(0)             //MSnB:MSnA = 1:0 Para PWM alineado alos lados
					  | FTM_CnSC_ELSB(0) |  FTM_CnSC_ELSA(1)            //ELSnB:ELSnA = 0:1 Para pulsos bajos como true
					  | FTM_CnSC_CHIE(0);                               //Activa las interrupciones por evento del canal
			FTM1_C1V  |= (TiempoEnCuentas * AnchoPulso);
			break;
		}


		break;
	}


	NVIC_EnableIRQ(FTM0_IRQn);                                  //Habilita el IRQ handler.
}


/********************************************************************************************************************************
 *Nombre:        FTM0_IRQHandler
 *
 *Definicion:    Rutina de interrupcion, sirve para borrar las banderas TOF y CH0F, ademas de ejercer accion sobre el pin
 *
 *Entradas:      Ninguna.
 *
 *Salidas:       Ninguna.
 ********************************************************************************************************************************/
void FTM0_IRQHandler(void)
{
    //	    FTM0_C2SC;                                             //Borrado de bandera paso 1: Lectura Dummy del registro
	//      FTM0_C2SC   = 0;                                       //Borrado de bandera paso 2: 0 to CH0F
	//      FTM0_C2SC  |= FTM_CnSC_MSB(1) | FTM_CnSC_CHIE(0)       //Resetea ala configuracion previa
	//		           | FTM_CnSC_MSA(0)  | FTM_CnSC_ELSB(0)
	//			       | FTM_CnSC_ELSA(1);
    v1++;
	//if((FTM0_SC & FTM_SC_TOF_MASK) == 0)
	// {
	//      FTM0_SC;                                               //Borrado de bandera paso 1: Lectura Dummy del registro
	//      FTM0_SC     = 0;                                       //Borrado de bandera paso 2: 0 a TOF
	//      FTM0_SC    |= FTM_SC_CPWMS(0) | FTM_SC_CLKS(1)         //Resetea ala configuracion previa
	//	             | FTM_SC_PS(UnivPrescaler) | FTM_SC_TOIE(1);
    // }
}



/********************************************************************************************************************************
 *Nombre:        valor_milisegundo
 *
 *Definicion:    Calcula la cantidad de cuentas necesaria para generar la unidad unidad de tiempo selecciinada, por el usuario
 *
 *Entradas:      UnidadDeTiempo. Frecuencia en Hz de la unidad de tiempo seleccionada por el usuario
 *               UnivPrescaler. Prescaler mas adecuado para la unidad de tiempo seleccionada
 *
 * Salidas:      milisegundo. the number of clock pulses to generate a milisecond
 ********************************************************************************************************************************/
UINT16 Calcula_Cuentas_Por_Tiempo(UINT32 UnidadDeTiempo, UINT8 UnivPrescaler)
{
	UINT8 PS;

	switch(UnivPrescaler)
	{
	case 0:
		PS = 1;
		break;
	case 7:
		PS = 128;
		break;
	}

	int SystemClock;
	short Cuentas;
	SystemClock = (((DEFAULT_SYSTEM_CLOCK)/PS)/UnidadDeTiempo)+1;   //DEFAULT_SYSTEM_CLOCK = core clock value (Default)
	                                                                //PS = valor del prescaler
	                                                                //UnidadDeTiempo = an valor en frecuencia de la unidad
	                                                                //de tiempo seleccionada.
	Cuentas = (short)SystemClock;
	return  Cuentas;
}


/********************************************************************************************************************************
 *Name:          Selecciona_Prescaler.
 *
 *Definicion:    En base a la unidad de tiempo seleccionada por el usuario.
 *
 *Entradas:      UnidadDeTiempo. Frecuencia en Hz de la unidad de tiempo seleccionada por el usuario.
 *
 *Salidas:       Prescaler. El prescaler adecuado segun la unidad de tiempo seleccionada.
 ********************************************************************************************************************************/
UINT32  Selecciona_Prescaler(UINT32 UnidadDeTiempo)
{
	UINT8 Prescaler;

	if(UnidadDeTiempo == MiliS)
	{
		Prescaler = 7;
		return Prescaler;
	}
	else
	{
		Prescaler = 0;
	    return Prescaler;
	}
}

