/*
#############################################################################################################

  Archivo      : GPIO.h
  Proyecto     : Modulo Interfaz K
  Fecha Inicio : 02/Marzo/2017


=============================================================================================================

  (c) Copyright SICOM, Sistemas y Controles MINED S.A. de C.V.

  SICOM, Sistemas y Controles MINED S.A. de C.V.

  http    : www.sicom.mx
  mail    : info@sicom.com

#############################################################################################################

 GPIO

#############################################################################################################
*/


#ifndef __GPIO_h_
#define __GPIO_h_

#include "Types.h"

//----------------------------------------------------------------------------
//
// Reloj
//
//----------------------------------------------------------------------------

#define GPIO_SELECTOR_RELOJ                     SIM_SCGC5
#define GPIO_PORTA_MASK                         SIM_SCGC5_PORTA_MASK
#define GPIO_PORTB_MASK                         SIM_SCGC5_PORTB_MASK
#define GPIO_PORTC_MASK                         SIM_SCGC5_PORTC_MASK
#define GPIO_PORTD_MASK                         SIM_SCGC5_PORTD_MASK
#define GPIO_PORTE_MASK                         SIM_SCGC5_PORTE_MASK

//----------------------------------------------------------------------------
//
// Declaracion de Estructuras de datos
//
//----------------------------------------------------------------------------

typedef enum
{
  eeb_Apagado = 0,
  eeb_Encendido,
}eEstadoBit;

typedef enum
{
  edpm_Entrada = 0,
  edpm_Salida,

  edpm_Ultimo,
}eDireccionPuertosMicro;

typedef enum
{
  eepu_Deshabilitar = 0,
  eepu_Habilitar,
}eEstadoPullUp;

typedef enum
{
  eeai_Deshabilitar = 0,
  eeai_Habilitar,
}eEstadoAltaImpedancia;

typedef enum
{
  epm_PuertoA = 0,
  epm_PuertoB,
  epm_PuertoC,
  epm_PuertoD,
  epm_PuertoE,

  emp_Ultimo,
}ePuertosMicro;

typedef enum
{
  ebpm_Bit0 = 0,
  ebpm_Bit1,
  ebpm_Bit2,
  ebpm_Bit3,
  ebpm_Bit4,
  ebpm_Bit5,
  ebpm_Bit6,
  ebpm_Bit7,
  ebpm_Bit8,
  ebpm_Bit9,
  ebpm_Bit10,
  ebpm_Bit11,
  ebpm_Bit12,
  ebpm_Bit13,
  ebpm_Bit14,
  ebpm_Bit15,
  ebpm_Bit16,
  ebpm_Bit17,
  ebpm_Bit18,
  ebpm_Bit19,
  ebpm_Bit20,
  ebpm_Bit21,
  ebpm_Bit22,
  ebpm_Bit23,
  ebpm_Bit24,
  ebpm_Bit25,
  ebpm_Bit26,
  ebpm_Bit27,
  ebpm_Bit28,
  ebpm_Bit29,
  ebpm_Bit30,
  ebpm_Bit31,

  ebpm_Ultimo,

}eBitsPuertoMicro;

//----------------------------------------------------------------------------
//
// Prototipos de funciones
//
//----------------------------------------------------------------------------
UINT8      GPIO_InicializarBit            (eDireccionPuertosMicro Direccion, ePuertosMicro Puerto, eBitsPuertoMicro Bit);
UINT8      GPIO_PullUp                    (eEstadoPullUp eActivar, ePuertosMicro Puerto, eBitsPuertoMicro Bit);
UINT8      GPIO_PullDown                  (eEstadoPullUp eActivar, ePuertosMicro Puerto, eBitsPuertoMicro Bit);
UINT8      GPIO_AltaImpedancia            (ePuertosMicro Puerto, eBitsPuertoMicro Bit);
void       GPIO_SetBitStatus              (ePuertosMicro Puerto, eBitsPuertoMicro Bit, eEstadoBit Estado);
UINT8      GPIO_GetBitStatus              (ePuertosMicro Puerto, eBitsPuertoMicro Bit);
void       GPIO_ToggleOutputStatus        (ePuertosMicro Puerto, eBitsPuertoMicro Bit);
UINT8      GPIO_ConfigurarDireccionBit    (eDireccionPuertosMicro Direccion, ePuertosMicro Puerto, eBitsPuertoMicro Bit);
UINT8      GPIO_Alt3                      (ePuertosMicro Puerto, eBitsPuertoMicro Bit);
#endif
